// You will need to stitch the ~9 second .ts chunks back together, though.

var childProcess = require("child_process");
var url = require("url");

var baseUrl = process.argv[2];
var start = parseInt(process.argv[3]);
var currentId = start;

// node /home/sven/get-at5/get-at5.js 'http://lb-rtvnh-live.streamgate.nl/at5/video/at5.smil/media_w1692481843_b2500000_{}.ts' 129804

function tryGetChunk() {
	var results = childProcess.spawnSync("wget", [baseUrl.replace("{}", currentId)]);
	
	if (results.status === 0) {
		console.log("Retrieved chunk", currentId);
		currentId += 1;
	} else {
		console.log("Retry in 5 secs...", currentId)
	}

	setTimeout(tryGetChunk, 5000);
}

tryGetChunk();